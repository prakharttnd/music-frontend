(function () {
    MyApp.controller('PlaylistController', PlaylistController);

    PlaylistController.$inject = ['PlaylistService'];

    function PlaylistController(PlaylistService) {
        var playlistCtrl = this;

        playlistCtrl.getPlaylists = function () {
            PlaylistService.getPlaylists(playlistCtrl);
        };

        playlistCtrl.getPlaylists();
    }
})();