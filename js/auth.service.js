(function () {
    'use strict';

    MyApp.factory('Auth', authService);

    authService.$inject = ['$http', '$state', '$rootScope', 'sessionStorageService', 'API'];

    function authService($http, $state, $rootScope, sessionStorageService, API) {
        var authDetails = {};

        function login(user, callback) {
            $http.post(API.BASE + "/login", user).then(function (response) {
                if (response.data.status == 1) {
                    authDetails = response.data.data;
                    $rootScope.currentUser = authDetails;
                    sessionStorageService.set("currentUser", authDetails);
                    $rootScope.$broadcast("CURRENT_USER_UPDATED");
                    callback(null);
                } else {
                    callback(response.data.message);
                    authDetails = {};
                }
            }, function () {
                callback("Something went wrong");
                authDetails = {};
            });
        }

        function getAuthDetails() {
            authDetails = authDetails && authDetails.username ? authDetails : sessionStorageService.get('currentUser');
            return authDetails;
        }

        function isLoggedIn() {
            getAuthDetails();
            return authDetails ? true : false;
        }

        function logout() {
            authDetails = {};
            $rootScope.currentUser = "";
            sessionStorageService.remove("currentUser");
            $state.go('login');
            $rootScope.$broadcast("CURRENT_USER_UPDATED");
        }

        return {
            login: login,
            getAuthDetails: getAuthDetails,
            isLoggedIn: isLoggedIn,
            logout: logout
        }
    }
})();