(function () {
    'use strict';

    MyApp.controller('TrackController', TrackController);

    TrackController.$inject = ['TrackService', 'PlaylistService'];

    function TrackController(TrackService, PlaylistService) {
        var trackCtrl = this;

        trackCtrl.playlistName = "";

        trackCtrl.getTracks = function () {
            TrackService.getTracks(trackCtrl);
        };

        trackCtrl.getPlaylists = function () {
            PlaylistService.getPlaylists(trackCtrl);
        };

        trackCtrl.getTracks();
        trackCtrl.getPlaylists();

        trackCtrl.addToPlaylist = function (trackObj) {
            trackCtrl.message = "";
            trackCtrl.trackObj = trackObj;
        };

        trackCtrl.checkAlreadyAdded = function (playlistId) {
            var index = $.inArray(playlistId, trackCtrl.trackObj.playListIds)
            return (index == -1 ? false : true)
        };

        trackCtrl.toggleTrackFromPlaylist = function (playlistId) {
            TrackService.toggleTrackFromPlaylist(playlistId, trackCtrl.trackObj.id, function (response) {
                if (response.data.status) {
                    var isChecked = $("#playlist_" + playlistId).is(":checked");
                    if (isChecked) {
                        trackCtrl.message = "Track added successfully";
                        trackCtrl.messageClass = "success";
                    } else {
                        trackCtrl.message = "Track removed successfully";
                        trackCtrl.messageClass = "error";
                    }
                } else {
                    trackCtrl.message = "Something went wrong";
                    trackCtrl.messageClass = "error";
                }
            });
        };

        trackCtrl.createPlaylist = function (playlistName) {
            PlaylistService.createPlaylist(playlistName, function (response) {
                if (response) {
                    $("#name").val("");
                    trackCtrl.playlists.push(response);
                } else {
                    trackCtrl.error = "Something went wrong";
                }
            });
        };
    }
})();