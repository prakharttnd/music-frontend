(function () {
    'use strict';

    MyApp.factory('TrackService', trackService);

    trackService.$inject = ['$http', 'API', 'Auth'];

    function trackService($http, API, Auth) {
        var user = Auth.getAuthDetails();

        function getTracks(scope) {
            $http.get(API.BASE + "/tracks/" + user.id).then(function (response) {
                scope.tracks = response.data;
            }, function () {
                console.log("Something went wrong");
            });
        }

        function toggleTrackFromPlaylist(playlistId, trackId, callback) {
            $http.post(API.BASE + "/toggleTrackFromPlaylist/" + user.id, {
                playlistId: playlistId,
                trackId: trackId
            }).then(function (response) {
                callback(response);
            }, function () {
                callback("Something went wrong");
            });
        }

        return {
            getTracks: getTracks,
            toggleTrackFromPlaylist: toggleTrackFromPlaylist
        }
    }
})();