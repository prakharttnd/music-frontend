(function () {
    'use strict';

    MyApp.controller("HeaderController", HeaderController);

    HeaderController.$inject = ['$rootScope', 'Auth'];

    function HeaderController($rootScope, Auth) {
        var header = this;

        header.isLoggedIn = Auth.isLoggedIn();

        header.logout = function () {
            Auth.logout();
        };

        $rootScope.$on('CURRENT_USER_UPDATED', function () {
            header.isLoggedIn = Auth.isLoggedIn();
        });
    }
})();
